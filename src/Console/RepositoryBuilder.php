<?php

namespace ApiBuilder\RepositoryBuilder\Console;

use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Illuminate\Console\Command;
use ApiBuilder\RepositoryBuilder\Builder\ModelBuilder;
use ApiBuilder\RepositoryBuilder\Builder\MigrationBuilder;
use ApiBuilder\RepositoryBuilder\Builder\EloquentRepositoryBuilder;
use ApiBuilder\RepositoryBuilder\Builder\InterfaceRepositoryBuilder;

class RepositoryBuilder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:repo {name} {--mo} {--mi} {--a}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new repository classes EloquentName and INameRepo. ';
    
    protected $bar;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        $this->comment($this->argument('name'));
        
        $progress = $this->option('a') ? 4 : ($this->option('mo') ? 3 : ($this->option('mi') ? 3 : 2) );
        $this->bar = $this->output->createProgressBar($progress);
        //$this->bar->setFormatDefinition('custom', ' %current%/%max% -- %message%');
        //$this->bar->setFormat('custom');

        $this->bar->setMessage('Preparing files...');
        $this->bar->start();
        if ($this->option('a')) {
            $this->input->setOption('mo', true);
            $this->input->setOption('mi', true);
        }
        if ($this->option('mo')) {
            $this->createModel();
        }
        if ($this->option('mi')) {
            $this->createMigration();
        }      
        $this->createRepository();

    }
    
    /**
     * Create a model file for the repository.
     *
     * @return void
     */
    protected function createModel()
    {
        (new ModelBuilder($this->argument('name')))->save();  
        $this->bar->setMessage('Creating Model');
        $this->bar->advance();
    }
    
    /**
     * Create a migration file for the model.
     *
     * @return void
     */
    protected function createMigration()
    {
        (new MigrationBuilder($this->argument('name')))->save();
        $this->bar->setMessage('Creating Migration');
        $this->bar->advance();
    }
    
    /**
     * Create a repository.
     *
     * @return void
     */
    protected function createRepository()
    {
        (new EloquentRepositoryBuilder($this->argument('name')))->save();
        $this->bar->setMessage('Creating Repository Eloquent');
        $this->bar->advance();
        (new InterfaceRepositoryBuilder($this->argument('name')))->save();
        $this->bar->setMessage('Creating Repository Interface');
        $this->bar->advance();
        $this->bar->finish();
    }
    
    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['all', 'a', InputOption::VALUE_NONE, 'Generate a migration and model for the repository'],
            
            ['model', 'mo', InputOption::VALUE_NONE, 'Create a new migration file for the repository.'],

            ['migration', 'mi', InputOption::VALUE_NONE, 'Create a new migration file for the model.'],

        ];
    }
    

}
