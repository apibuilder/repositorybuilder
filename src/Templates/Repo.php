<?php

namespace App\Repositories\{{model}};

use ApiBuilder\RepositoryBuilder\Repositories\IRepo;
/**
 * Interface {{model}}Repository
 * @package App\Repositories\{{model}}
 */
interface I{{model}}Repo extends IRepo {
   
    
}